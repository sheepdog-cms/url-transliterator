<?php
/*
 * Url Transliterator
 *
 * @package Sheepdog\Extensions\UrlTransliterator
 * @since   0.0.9
*/
namespace Sheepdog\Extensions\UrlTransliterator;

class BeforeMatchLayer {
    public function Run () {
        $_SERVER['REQUEST_URI'] = transliterate(urldecode($_SERVER['REQUEST_URI']));
    }
}